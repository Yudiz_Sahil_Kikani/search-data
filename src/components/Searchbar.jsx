import { React, useState, useEffect } from 'react'
import Brand from './Brand.json'

function Searchbar () {
  const [search, setSearch] = useState('')
  const [data, setData] = useState(Brand)

  const searchdata = (event) => { setSearch(event.target.value) }

  useEffect(() => {
    if (search) {
      const deley = setTimeout(() => {
        setData(Brand.filter((value) => {
          if (search === ' ') {
            return value
          } else {
            return value.name.toLowerCase().includes(search.toLowerCase())
          }
        }))
      }, 3000)
      return () => { clearTimeout(deley) }
    } else {
      setData(Brand.filter((value) => {
        if (search === ' ') {
          return value
        } else {
          return value.name.toLowerCase().includes(search.toLowerCase())
        }
      }))
    }
  }, [search])

  return (

    <div className="App">
      <h1>Search Bar</h1>
      <input
        type="text"
        placeholder="Searh..."
        onChange={searchdata}
      />
      {data.map((value, key) => {
        return (
                     <div className="brand" key={key}>
                           <p>{value.name}</p>
                     </div>
        )
      })
    }
    </div>
  )
}

export default Searchbar
